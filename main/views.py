# coding=utf-8
from annoying.decorators import render_to
from django.shortcuts import get_object_or_404
from django.views.generic import ListView, DetailView
from models import ContentItem, Rubric, Main, ContestItem, ImageItem, KonkursItem

class ViewPost(DetailView):
    model               = ContentItem
    context_object_name = 'post'
    template_name       = 'post.html'
    def get_object(self):
        object = super(ViewPost, self).get_object()
        return object

class ViewKonkursItem(DetailView):
    model               = KonkursItem
    context_object_name = 'post'
    template_name       = 'post.html'
    def get_object(self):
        object = super(ViewKonkursItem, self).get_object()
        return object

class RubricItemsView(ListView):
    paginate_by         = 5
    template_name       = 'rubric.html'
    context_object_name = 'posts'
    def get_queryset(self):
        self.rubric = get_object_or_404(Rubric, slug=self.kwargs['rubric'])
        return ContentItem.objects.filter(rubric=self.rubric)

@render_to('index.html')
def home(request):
    header     = Main.objects.latest('pub_date')
    last_posts = ContentItem.objects.all()[:6]
    return {
        'header': header,
        'posts' : last_posts,
    }

class LastContestItemsView(ListView):
    template_name       = 'contest.html'
    context_object_name = 'posts'
    def get_queryset(self):
        self.contest = ContestItem.objects.latest('pub_date')
        return KonkursItem.objects.filter(konkurs=self.contest)	
   	
    def get_context_data(self, **kwargs):
        context = super(LastContestItemsView, self).get_context_data(**kwargs)
        context['konkurs'] = ContestItem.objects.latest('pub_date')
        return context	

class AllContestItemsView(ListView):
    template_name       = 'all_contest.html'
    context_object_name = 'posts'
    def get_queryset(self):
        return ContestItem.objects.all()
		
class AllContentItemsView(ListView):
    template_name       = 'all_content.html'
    context_object_name = 'posts'
    paginate_by         = 50
    def get_queryset(self):
        return ContentItem.objects.all()

class ContestItemsView(ListView):
    template_name       = 'contest.html'
    context_object_name = 'posts'
    def get_queryset(self):
        self.contest = get_object_or_404(ContestItem, slug=self.kwargs['konkurs'])
        return KonkursItem.objects.filter(konkurs=self.contest)

class ImageItemsView(ListView):
    template_name       = 'images.html'
    context_object_name = 'images'
    def get_queryset(self):
        self.rubric = get_object_or_404(Rubric, slug=self.kwargs['rubric'])
        return ImageItem.objects.filter(rubric=self.rubric)