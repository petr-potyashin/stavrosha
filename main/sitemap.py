﻿from django.contrib.sitemaps import Sitemap
from main.models import ContentItem
from django.core.urlresolvers import reverse

class SitemapXML(Sitemap):
    changefreq = "never"
    priority = 0.5

    def items(self):
        return ContentItem.objects.all()

    def lastmod(self, obj):
        return obj.pub_date

	def location(self, obj):
		return "/content/%s/" % obj.slug

class ViewSitemap(Sitemap):

	changefreq = "never"
	priority = 0.5

	def items(self):
		return ['home']
		
	def location(self, item):
		return reverse(item)

sitemaps = {'views': ViewSitemap, 'content': SitemapXML}