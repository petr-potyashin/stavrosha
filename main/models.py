﻿# coding=utf-8
from django.db import models
from datetime import datetime
from django.db.models.signals import post_save, pre_delete
from utils import *

class Rubric(models.Model):
    class Meta:
        verbose_name=u'Рубрика'
        verbose_name_plural=u'Рубрики'

    title = models.CharField(max_length=250, verbose_name=u'Заголовок')
    slug = models.SlugField(unique=True)
    image = models.ImageField(verbose_name=u'Изображение', upload_to='images/rubric_images/', blank=True)
    content = models.TextField(verbose_name=u'Текстовое описание', blank=True)

    def __unicode__(self):
        return self.title

    @property
    def thumbnail(self):
        return get_thumbnail_url(self.image.url)

    @models.permalink
    def get_absolute_url(self):
        return ( 'rubric_view', (), {'rubric': self.slug})



class ImageItem(models.Model):
    class Meta:
        verbose_name=u'Изображение'
        verbose_name_plural=u'Изображения'

    title = models.CharField(max_length=250, verbose_name=u'Описание', blank=True)
    image = models.ImageField(verbose_name=u'Изображение', upload_to='images/image_item/')
    rubric = models.ForeignKey(Rubric, verbose_name=u'Рубрика')
    download_url = models.CharField(max_length=250, verbose_name=u'Ссылка на скачивание')

    def __unicode__(self):
        return self.title

    @property
    def thumbnail(self):
        return get_thumbnail_url(self.image.url)

def post_save_handler(sender, **kwargs):
    create_thumbnail(kwargs['instance'].image.path)
post_save.connect(post_save_handler, sender=ImageItem)

def pre_delete_handler(sender, **kwargs):
    delete_thumbnail(kwargs['instance'].image.path)
pre_delete.connect(pre_delete_handler, sender=ImageItem)

class ContentItem(models.Model):
    class Meta:
        ordering = ['-pub_date']
        verbose_name=u'Статья'
        verbose_name_plural=u'Статьи'

    title = models.CharField(max_length=250, verbose_name=u'Заголовок', blank=True)
    slug = models.SlugField(unique=True, blank=True)
    rubric = models.ForeignKey(Rubric, verbose_name=u'Рубрика')
    pub_date = models.DateTimeField(verbose_name=u'Дата публикации', default=datetime.now)
    content = models.TextField(verbose_name=u'Содержимое')

    def __unicode__(self):
        return u'%s (%s)' % (self.title,self.pub_date)

    @models.permalink
    def get_absolute_url(self):
        return ('view_post', (), {'slug': self.slug})

    @property
    def get_trancate_text(self):
        try:
            str = self.content.split("---")
            if str[1]: str[0] += "..."
            return str[0]
        except:
            return self.content

    @property
    def get_full_text(self):
        return self.content.replace("---", "", 1)

class ContestItem(models.Model):
    class Meta:
        ordering = ['-pub_date']
        verbose_name=u'Конкурс'
        verbose_name_plural=u'Конкурсы'

    title = models.CharField(max_length=250, verbose_name=u'Заголовок', blank=True)
    slug = models.SlugField(unique=True, blank=True)
    pub_date = models.DateTimeField(verbose_name=u'Дата публикации', default=datetime.now)
    content = models.TextField(verbose_name=u'Содержимое')
    
    def __unicode__(self):
        return u'%s' %self.title

    @models.permalink
    def get_absolute_url(self):
        return ( 'contest_view', (), {'contest': self.slug})

class Main(models.Model):
    class Meta:
        ordering = ['-pub_date']
        verbose_name=u'Для главной'
        verbose_name_plural=u'Для главной'

    image = models.ImageField(verbose_name=u'Изображение', upload_to='images/main_images/')
    pub_date = models.DateTimeField(verbose_name=u'Дата публикации', default=datetime.now)

    def __unicode__(self):
        return u'%s' % (self.pub_date)
    	
class KonkursItem(models.Model):
    class Meta:
        ordering = ['-pub_date']
        verbose_name=u'Конкурсная статья'
        verbose_name_plural=u'Конкурсные статьи'

    title = models.CharField(max_length=250, verbose_name=u'Заголовок', blank=True)
    slug = models.SlugField(unique=True, blank=True)
    konkurs = models.ForeignKey(ContestItem, verbose_name=u'Конкурс')
    pub_date = models.DateTimeField(verbose_name=u'Дата публикации', default=datetime.now)
    content = models.TextField(verbose_name=u'Содержимое')

    def __unicode__(self):
        return u'%s (%s)' % (self.title,self.konkurs)

    @models.permalink
    def get_absolute_url(self):
        return ('view_konkurs', (), {'slug': self.slug})

    @property
    def get_trancate_text(self):
        try:
            str = self.content.split("---")
            if str[1]: str[0] += "..."
            return str[0]
        except:
            return self.content

    @property
    def get_full_text(self):
        return self.content.replace("---", "", 1)
