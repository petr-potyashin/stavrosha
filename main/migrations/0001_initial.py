# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Rubric'
        db.create_table('main_rubric', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=250)),
            ('slug', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=50)),
            ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=100, blank=True)),
        ))
        db.send_create_signal('main', ['Rubric'])

        # Adding model 'ImageItem'
        db.create_table('main_imageitem', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=250, blank=True)),
            ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('rubric', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['main.Rubric'])),
            ('download_url', self.gf('django.db.models.fields.CharField')(max_length=250)),
        ))
        db.send_create_signal('main', ['ImageItem'])

        # Adding model 'ContentItem'
        db.create_table('main_contentitem', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=250, blank=True)),
            ('slug', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=50, blank=True)),
            ('rubric', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['main.Rubric'])),
            ('pub_date', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('content', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('main', ['ContentItem'])

        # Adding model 'ContestItem'
        db.create_table('main_contestitem', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=250, blank=True)),
            ('pub_date', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('content', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('main', ['ContestItem'])

        # Adding model 'Main'
        db.create_table('main_main', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('pub_date', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
        ))
        db.send_create_signal('main', ['Main'])

        # Adding model 'KonkursItem'
        db.create_table('main_konkursitem', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=250, blank=True)),
            ('slug', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=50, blank=True)),
            ('konkurs', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['main.ContestItem'])),
            ('pub_date', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('content', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('main', ['KonkursItem'])


    def backwards(self, orm):
        # Deleting model 'Rubric'
        db.delete_table('main_rubric')

        # Deleting model 'ImageItem'
        db.delete_table('main_imageitem')

        # Deleting model 'ContentItem'
        db.delete_table('main_contentitem')

        # Deleting model 'ContestItem'
        db.delete_table('main_contestitem')

        # Deleting model 'Main'
        db.delete_table('main_main')

        # Deleting model 'KonkursItem'
        db.delete_table('main_konkursitem')


    models = {
        'main.contentitem': {
            'Meta': {'ordering': "['-pub_date']", 'object_name': 'ContentItem'},
            'content': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pub_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'rubric': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['main.Rubric']"}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'})
        },
        'main.contestitem': {
            'Meta': {'ordering': "['-pub_date']", 'object_name': 'ContestItem'},
            'content': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pub_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'})
        },
        'main.imageitem': {
            'Meta': {'object_name': 'ImageItem'},
            'download_url': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'rubric': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['main.Rubric']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'})
        },
        'main.konkursitem': {
            'Meta': {'ordering': "['-pub_date']", 'object_name': 'KonkursItem'},
            'content': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'konkurs': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['main.ContestItem']"}),
            'pub_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'})
        },
        'main.main': {
            'Meta': {'ordering': "['-pub_date']", 'object_name': 'Main'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'pub_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'})
        },
        'main.rubric': {
            'Meta': {'object_name': 'Rubric'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '250'})
        }
    }

    complete_apps = ['main']