# coding=utf-8
from django.contrib import admin
from main.models import ContentItem, Rubric, Main, ContestItem, ImageItem, KonkursItem

class ContentItemAdmin(admin.ModelAdmin):
    class Media:
        js = [
            '/static/grappelli/tinymce/jscripts/tiny_mce/tiny_mce.js',
            '/static/grappelli/tinymce_setup/tinymce_setup.js',
            ]

    list_display = ('title', 'pub_date', 'rubric')
    list_filter = ('pub_date', 'rubric',)
    prepopulated_fields = {'slug': ('title',)}

class KonkursItemAdmin(admin.ModelAdmin):
    class Media:
        js = [
            '/static/grappelli/tinymce/jscripts/tiny_mce/tiny_mce.js',
            '/static/grappelli/tinymce_setup/tinymce_setup.js',
            ]

    list_display = ('title', 'pub_date', 'konkurs')
    list_filter = ('pub_date', 'konkurs',)
    prepopulated_fields = {'slug': ('title',)}	

class ContestItemAdmin(admin.ModelAdmin):
    class Media:
        js = [
            '/static/grappelli/tinymce/jscripts/tiny_mce/tiny_mce.js',
            '/static/grappelli/tinymce_setup/tinymce_setup.js',
            ]

    list_display = ('title', 'pub_date')
    list_filter = ('pub_date',)
    prepopulated_fields = {'slug': ('title',)}

class RubricAdmin(admin.ModelAdmin):
    list_display = ('title', 'image',)
    prepopulated_fields = {'slug': ('title',)}
    class Media:
        js = [
            '/static/grappelli/tinymce/jscripts/tiny_mce/tiny_mce.js',
            '/static/grappelli/tinymce_setup/tinymce_setup.js',
            ]

class MainAdmin(admin.ModelAdmin):
    class Media:
        js = [
            '/static/grappelli/tinymce/jscripts/tiny_mce/tiny_mce.js',
            '/static/grappelli/tinymce_setup/tinymce_setup.js',
            ]

    list_filter = ('pub_date',)

class ImageItemAdmin(admin.ModelAdmin):
    list_display = ('title', 'image', 'rubric',)


admin.site.register(ContentItem, ContentItemAdmin)
admin.site.register(KonkursItem, KonkursItemAdmin)
admin.site.register(ContestItem, ContestItemAdmin)
admin.site.register(Rubric, RubricAdmin)
admin.site.register(Main, MainAdmin)
admin.site.register(ImageItem, ImageItemAdmin)