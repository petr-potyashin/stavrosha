# coding=utf-8
from django.contrib import admin
from models import QuizItem, Quiz

class QuizAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}

class QuizItemAdmin(admin.ModelAdmin):
    list_filter = ('quiz',)
    list_display = ('question', 'quiz',)
	
admin.site.register(QuizItem, QuizItemAdmin)
admin.site.register(Quiz, QuizAdmin)
