# coding=utf-8
from django.shortcuts import get_object_or_404
from django.views.generic import ListView
from games.models import Quiz, QuizItem

class QuizView(ListView):
    template_name       = 'games/quiz.html'
    context_object_name = 'posts'
    def get_queryset(self):
        self.quiz = get_object_or_404(Quiz, slug=self.kwargs['quiz'])
        return QuizItem.objects.filter(quiz=self.quiz)

class AllQuizView(ListView):
    template_name       = 'games/all_quiz.html'
    context_object_name = 'posts'
    def get_queryset(self):
        return Quiz.objects.all()