# coding=utf-8
from django.db import models

class Quiz(models.Model):
    class Meta:
        verbose_name=u'Викторина'
        verbose_name_plural=u'Викторины'

    title = models.CharField(max_length=250, verbose_name=u'Заголовок')
    slug = models.SlugField(unique=True)

    def __unicode__(self):
        return self.title

    @models.permalink
    def get_absolute_url(self):
        return ( 'quiz_view', (), {'quiz': self.slug})

class QuizItem(models.Model):
    class Meta:
        verbose_name=u'Вопрос'
        verbose_name_plural=u'Вопросы'

    quiz = models.ForeignKey(Quiz, verbose_name=u'Викторина')    
    question = models.CharField(max_length=250, verbose_name=u'Вопрос', blank=True)
    answer = models.CharField(max_length=250, verbose_name=u'Ответ')

    def __unicode__(self):
        return self.question