﻿from django.conf.urls import patterns, include, url
from filebrowser.sites import site
from main import views
from settings import MEDIA_ROOT
from django.contrib import admin
from django.views.generic import TemplateView
from main.sitemap import sitemaps
from django.conf.urls.defaults import *
admin.autodiscover()

from games.views import AllQuizView, QuizView

class PlainTextTemplateView(TemplateView):
    def render_to_response(self, context, **kwargs):
        return super(PlainTextTemplateView, self).render_to_response(
            context,
            content_type='text/plain',
            **kwargs
        )

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^admin/filebrowser/', include(site.urls)),

    url(r'^$', views.home, name="home"),
    url(r'^rubric/(?P<rubric>[-\w]+)/$', views.RubricItemsView.as_view(), name='rubric_view' ),
    url(r'^rubric/(?P<rubric>[-\w]+)/page(?P<page>\d+)/$', views.RubricItemsView.as_view() ),
    url(r'^content/(?P<slug>[-\w]+)/$', views.ViewPost.as_view(), name='view_post' ),
    url(r'^content/all/page(?P<page>\d+)/$', views.AllContentItemsView.as_view() ),
    url(r'^konkursy/$', views.LastContestItemsView.as_view() ),
    url(r'^konkursy/all$', views.AllContestItemsView.as_view() ),
    url(r'^konkursy/(?P<konkurs>[-\w]+)/$', views.ContestItemsView.as_view(), name='contest_view' ),
    url(r'^konkursy/content/(?P<slug>[-\w]+)/$', views.ViewKonkursItem.as_view(), name='view_konkurs'),
    (r'^about-journal/$', TemplateView.as_view(template_name="about-journal.html")),
    (r'^about-us/$', TemplateView.as_view(template_name="about-us.html")),
    (r'^about-subscription/$', TemplateView.as_view(template_name="about-subscription.html")),
    url(r'^images/(?P<rubric>[-\w]+)/$', views.ImageItemsView.as_view()),
    
    url(r'^robots\.txt$', PlainTextTemplateView.as_view(template_name='robots.txt')),
    (r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': sitemaps}),

    url(r'^f9a03f70-46f9-49f9-88cc-5a3481d6ed13\.txt$', PlainTextTemplateView.as_view(template_name='f9a03f70-46f9-49f9-88cc-5a3481d6ed13.txt')),

    #(r'^games/$', TemplateView.as_view(template_name="games/games.html")),
    #url(r'^games/prover-svoi-znaniya/$', AllQuizView.as_view(), name='all_quiz_view'),
    #url(r'^games/prover-svoi-znaniya/(?P<quiz>[-\w]+)/$', QuizView.as_view(), name='quiz_view' ),
)

###
urlpatterns += patterns('',
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': MEDIA_ROOT,
    }),
)
###